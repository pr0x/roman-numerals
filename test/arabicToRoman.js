const should = require('chai').should();
const arabicToRoman = require('../src/arabicToRoman');
const testCases = require('./equality-cases');

const badInputs = [
  0,
  -1,
  3.14,
  'string',
  NaN,
  4000
];

describe('Arabic to Roman', () => {
  describe('Input Sanitisation', () => {
    badInputs.forEach(bad => {
      it(`should not allow ${bad}`, () => {
        (() => {
          arabicToRoman(bad);
        }).should.throw();
      });
    });
  });

  describe('Equality Checks', () => {
    testCases.forEach( ([arabic, roman]) => {
      it(`should return ${roman} for ${arabic}`, () => {
        arabicToRoman(arabic).should.equal(roman);
      });
    });
  });

  describe('Alternatives', () => {
    it(`should support the alternative form of 18 (XIIX)`, () => {
      arabicToRoman(18, true).should.equal('XIIX');
    });
    
    it(`should support the alternative form of 4 (IIII)`, () => {
      arabicToRoman(4, true).should.equal('IIII');
    });
    
    it(`should support the alternative form of 9 (VIIII)`, () => {
      arabicToRoman(9, true).should.equal('VIIII');
    });
    
    it(`should support the alternative form of 999 (DCCCCLXXXXVIIII)`, () => {
      arabicToRoman(999, true).should.equal('DCCCCLXXXXVIIII');
    });
  });
});
