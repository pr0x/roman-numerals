const equalityCases = [
  [1, 'I'],
  [2, 'II'],
  [3, 'III'],
  [4, 'IV'],
  [5, 'V'],
  [6, 'VI'],
  [7, 'VII'],
  [8, 'VIII'],
  [9, 'IX'],
  [10, 'X'],
  [11, 'XI'],
  [12, 'XII'],
  [13, 'XIII'],
  [14, 'XIV'],
  [15, 'XV'],
  [16, 'XVI'],
  [17, 'XVII'],
  [18, 'XVIII'],
  [19, 'XIX'],
  [20, 'XX'],
  [24, 'XXIV'],
  [25, 'XXV'],
  [26, 'XXVI'],
  [29, 'XXIX'],
  [30, 'XXX'],
  [40, 'XL'],
  [44, 'XLIV'],
  [49, 'XLIX'],
  [50, 'L'],
  [60, 'LX'],
  [70, 'LXX'],
  [80, 'LXXX'],
  [90, 'XC'],
  [94, 'XCIV'],
  [95, 'XCV'],
  [96, 'XCVI'],
  [99, 'XCIX'],
  [100, 'C'],
  [101, 'CI'],
  [109, 'CIX'],
  [110, 'CX'],
  [111, 'CXI'],
  [114, 'CXIV'],
  [144, 'CXLIV'],
  [149, 'CXLIX'],
  [150, 'CL'],
  [194, 'CXCIV'],
  [199, 'CXCIX'],
  [200, 'CC'],
  [300, 'CCC'],
  [400, 'CD'],
  [444, 'CDXLIV'],
  [500, 'D'],
  [600, 'DC'],
  [700, 'DCC'],
  [800, 'DCCC'],
  [900, 'CM'],
  [944, 'CMXLIV'],
  [999, 'CMXCIX'],
  [1000, 'M'],
  [1009, 'MIX'],
  [1044, 'MXLIV'],
  [1090, 'MXC'],
  [1100, 'MC'],
  [1111, 'MCXI'],
  [1444, 'MCDXLIV'],
  [1499, 'MCDXCIX'],
  [1500, 'MD'],
  [1666, 'MDCLXVI'],
  [1700, 'MDCC'],
  [1818, 'MDCCCXVIII'],
  [1919, 'MCMXIX'],
  [1944, 'MCMXLIV'],
  [1949, 'MCMXLIX'],
  [1999, 'MCMXCIX'],
  [2000, 'MM'],
  [2001, 'MMI'],
  [2999, 'MMCMXCIX'],
  [3000, 'MMM'],
  [3999, 'MMMCMXCIX'],
];

module.exports = equalityCases;
