const units = [
  { one: 'I', five: 'V', ten: 'X' }, // 1s
  { one: 'X', five: 'L', ten: 'C' }, // 10s
  { one: 'C', five: 'D', ten: 'M' }, // 100s
  { one: 'M' }, // 1000s
];

const alts = {
  18: 'XIIX'
};

const arabicToRoman = (arabic, alternative = false) => {
  if(Number(arabic) <= 0) {
    throw new Error('Input must be greater than zero.');
  }

  if(Number.isInteger(arabic) === false) {
    throw new Error('Input must be a whole number.');
  }

  if(isNaN(Number(arabic))) {
    throw new Error('Input must be a number.');
  }

  if(Number(arabic) > 3999) {
    throw new Error('Max number able to be represented in Roman is 3999 (MCMXCIX)');
  }

  let alt = false;
  if(alternative) {
    alt = lookupAlternativeNotation(arabic);
  }

  return alt || getStandardRomanNotation(arabic, alternative);
}

const lookupAlternativeNotation = (arabic) => {
  return alts[arabic] || false;
}

const getStandardRomanNotation = (arabic, alternative = false) => {
  return String(arabic)
    .split('')
    .reverse()
    .map( (n, i) => digitToRoman(n, units[i], alternative) )
    .reverse()
    .join('');
}

const digitToRoman = (digit, glyphs, alternative = false) => {
  let { one, five, ten } = glyphs;
  digit = Number(digit);

  switch(true) {
    case (digit === 9 && !alternative):
      return one + ten;

    case (digit > 4):
      return five + one.repeat(digit - 5);

    case (digit === 4 && !alternative):
      return one + five;

    default:
      return one.repeat(digit);
  }
}

module.exports = arabicToRoman
